package com.kytms.system.dao.Impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.Button;
import com.kytms.system.dao.ButtonDao;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 * 按钮DAO
 *
 * @author
 * @create 2017-11-20
 */
@Repository(value = "ButtonDao")
public class ButtonDaoImpl extends BaseDaoImpl<Button> implements ButtonDao<Button>{
}
