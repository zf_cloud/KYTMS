package com.kytms.system.dao.Impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.Notice;
import com.kytms.system.dao.NoticeDao;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 * 按钮DAO
 *
 * @author
 * @create 2017-11-20
 */
@Repository(value = "NoticeDao")
public class NoticeDaoImpl extends BaseDaoImpl<Notice> implements NoticeDao<Notice> {
}
